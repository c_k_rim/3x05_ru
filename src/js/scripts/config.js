const APP = {
  name: 'Schnur App',
  mobileWidth: 767,
  tabletWidth: 1366,
  desktopWidth: 1440,
  desktopHeight: 742,
};

// Нули перед числами < 10
const minTwoDigits = n => (n < 10 ? '0' : '') + n;

// Короткие алиасы для querySelector
const qs = el => document.querySelector(el);
const qsA = el => document.querySelectorAll(el);
