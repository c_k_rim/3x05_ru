document.addEventListener('DOMContentLoaded', () => {
  // Шаринг
  const Share = {
    vkontakte: (purl, ptitle, pimg, text) => {
      let url = 'http://vkontakte.ru/share.php?';
      url += 'url=' + encodeURIComponent(purl);
      url += '&title=' + encodeURIComponent(ptitle);
      url += '&description=' + encodeURIComponent(text);
      url += '&image=' + encodeURIComponent(pimg);
      url += '&noparse=true';

      Share.popup(url);
    },
    facebook: (purl, ptitle, pimg, text) => {
      console.log('fb share', purl, ptitle, pimg, text);
      FB.ui({
        method: 'share_open_graph',
        action_type: 'og.shares',
        action_properties: JSON.stringify({
          object: {
            'og:url': purl,
            'og:title': ptitle,
            'og:description': text,
            'og:image': pimg,
            'og:image:width': 1200,
            'og:image:height': 630
          }
        })
      }, function(response) {});
    },
    twitter: (purl, ptitle) => {
      let url = 'http://twitter.com/share?';
      url += 'text=' + encodeURIComponent(ptitle);
      url += '&url=' + encodeURIComponent(purl);
      url += '&counturl=' + encodeURIComponent(purl);
      Share.popup(url);
    },
    popup: url => {
      window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
    }
  };

  // Класс теста
  class Test {
    constructor() {
      this.current = 0;
      this.storage = [];
      this.testSel = '.js-test';
      this.testContentSel = '.js-test-content';
      this.radioSel = '.js-custom-radio';
      this.testResultsSel = '.js-test-results';

      this.test = [{
          id: 1,
          name: 'Сколько чистой воды ты пьешь в сутки?',
          img: '/assets/img/test/bottles.png',
          prefix: 'bottles',
          answer: [{
              3: 'Я соблюдаю питьевой режим, выпиваю 1,5 – 2 литра чистой воды в сутки'
            },
            {
              2: '2-4 стакана в день'
            },
            {
              1: 'Когда как — иногда вообще за весь день могу не выпить ни капли'
            },
            {
              0: 'Предпочитаю напитки «с градусами»'
            }
          ]
        },
        {
          id: 2,
          name: 'В основе твоего рациона...',
          img: '/assets/img/test/food.png',
          prefix: 'food',
          answer: [{
              3: 'Фрукты, овощи, злаки, рыба, мясо.'
            },
            {
              2: 'Хлебобулочные изделия, сладости.'
            },
            {
              1: 'Супы быстрого приготовления, чипсы, сухарики, сладости.'
            },
            {
              0: 'Сигаретный дым и алкоголь.'
            }
          ]
        },
        {
          id: 3,
          name: 'Сколько часов в неделю ты занимаешься спортом?',
          img: '/assets/img/test/dumbbells.png',
          prefix: 'dumbbells',
          answer: [{
              3: 'Более 4-х часов.'
            },
            {
              2: '1 – 4 часа.'
            },
            {
              1: 'Я не трачу время на глупости.'
            },
            {
              0: '«Литрболл» считается спортом?'
            }
          ]
        },
        {
          id: 4,
          name: 'Сколько часов в сутки (в среднем за неделю) ты спишь?',
          img: '/assets/img/test/alarm-clock.png',
          prefix: 'alarm-clock',
          answer: [{
              1: 'Менее 6 часов.'
            },
            {
              2: '6 – 7 часов.'
            },
            {
              3: 'Около 8 часов.'
            }
          ]
        },
        {
          id: 5,
          name: 'Как часто ты гуляешь на свежем воздухе?',
          img: '/assets/img/test/tree.png',
          prefix: 'tree',
          answer: [{
              3: 'Ежедневно.'
            },
            {
              2: 'Один раз в два – три дня.'
            },
            {
              1: 'Хорошо, если выйду раз в неделю.'
            },
            {
              0: 'На свежем воздухе я только во время перекура.'
            }
          ]
        }
      ];

      this.results = [{
          title: 'Ты&nbsp;Шнур года этак 1998-2001',
          img: '/assets/img/test/schnur-bad.jpg',
          imgTablet: '/assets/img/test/schnur-bad-tablet.jpg',
          imgMobile: '/assets/img/test/schnur-bad-mobile.jpg',
          shareImg: '/assets/img/test/schnur-bad-social.jpg',
          text: '&laquo;Градус&raquo; такой жизни хоть и&nbsp;высок (от&nbsp;40&nbsp;и&nbsp;выше), но&nbsp;не&nbsp;слишком греет&nbsp;&mdash; ни&nbsp;тебя самого, ни&nbsp;твоих близких. Так что, может, попробуешь по-другому? Чем ты&nbsp;рискуешь? Возьми и&nbsp;поучаствуй в&nbsp;программе &laquo;Недельный водопой&raquo; (подчеркиваем: ВОДОПОЙ)&nbsp;&mdash; спорим на&nbsp;билеты и&nbsp;гитару с&nbsp;автографом Шнура, что тебе понравится?'
        },
        {
          title: 'Соберись! А&nbsp;пока ты&nbsp;Шнур 2009&nbsp;года...',
          img: '/assets/img/test/schnur-normal.jpg',
          imgTablet: '/assets/img/test/schnur-normal-tablet.jpg',
          imgMobile: '/assets/img/test/schnur-normal-mobile.jpg',
          shareImg: '/assets/img/test/schnur-normal-social.jpg',
          text: 'Но, будем откровенны, бывало и&nbsp;хуже. Самое время обратить внимание на&nbsp;себя, начать восхождение к&nbsp;вершинам молодости, активности и&nbsp;здоровья. Самый простой способ для этого&nbsp;&mdash; участвовать в&nbsp;программе &laquo;Недельный водопой&raquo;, выиграть билеты на&nbsp;концерт группировки &laquo;Ленинград&raquo; или гитару с&nbsp;автографом Шнура.'
        },
        {
          title: 'Факт&nbsp;&mdash; ты&nbsp;Шнур 2015&nbsp;года.',
          img: '/assets/img/test/schnur-good.jpg',
          imgTablet: '/assets/img/test/schnur-good-tablet.jpg',
          imgMobile: '/assets/img/test/schnur-good-mobile.jpg',
          shareImg: '/assets/img/test/schnur-good-social.jpg',
          text: 'Достижения здорового образа жизни для вас еще впереди. Наступают светлые дни, темп выбран верный, главное не&nbsp;сорваться. А&nbsp;держаться проще в&nbsp;кругу единомышленников. Участвуй в&nbsp;программе &laquo;Недельный водопой&raquo; и&nbsp;выиграй билеты на&nbsp;концерт группировки &laquo;Ленинград&raquo; или гитару с&nbsp;автографом Шнура.'
        },
        {
          title: 'Ты&nbsp;в&nbsp;тренде! Настоящий Шнур 2019 года розлива!',
          img: '/assets/img/test/schnur-excellent.jpg',
          imgTablet: '/assets/img/test/schnur-excellent-tablet.jpg',
          imgMobile: '/assets/img/test/schnur-excellent-mobile.jpg',
          shareImg: '/assets/img/test/schnur-excellent-social.jpg',
          text: 'Великолепно! &laquo;Святой Источник&raquo; всем офисом завидует тебе и&nbsp;предлагает присоединиться к&nbsp;клубу таких&nbsp;же успешных здоровых людей. Да&nbsp;что там присоединиться&nbsp;&mdash; возглавить его! Смело участвуй в&nbsp;программе &laquo;Недельный водопой&raquo;, выиграй билеты на&nbsp;концерт группировки &laquo;Ленинград&raquo; или гитару с&nbsp;автографом Шнура.'
        }
      ];
    }

    handleEvents() {
      document.addEventListener('change', e => {
        const target = e.target;
        const targetClassList = target.classList;

        if (!targetClassList.contains('js-custom-radio')) {
          return;
        }

        this.setAnswer(Number(target.dataset.radioId));
      });

      document.addEventListener('click', e => {
        const target = e.target;
        const targetClassList = target.classList;

        if (!targetClassList.contains('js-test-btn')) {
          return;
        }

        this.setQuestion(targetClassList.contains('is-prev') ? this.current - 1 : this.current + 1);
      });

      document.addEventListener('click', e => {
        const target = e.target;
        const targetClassList = target.classList;

        if (!targetClassList.contains('js-reset-test-btn')) {
          return;
        }

        qs(this.testResultsSel).innerHTML = '';
        qs(this.testResultsSel).classList.remove('is-show');
        qs(this.testSel).classList.add('is-show');
      });

      $(document).on('click', '.js-share-item', function() {
        Share[$(this).data('share')](...$(this).data('params').split('&&'));
      });
    }

    reset() {
      this.storage = [];
      this.current = 0;
    }

    renderQuestion() {
      const currentQuestion = this.test[this.current];

      const template = `<div class="test__img-wrap test__img-wrap_${ currentQuestion.prefix }">
          <img src="${ currentQuestion.img }" alt="" class="test__img">
        </div>
        <div class="test__step test__step_${ currentQuestion.prefix }">
          <header class="test__step-head">
            <span class="test__question-count">Вопрос <span>${ this.current + 1}</span> из <span>${ this.test.length }</span></span>

            <h4 class="test__question-title">${ currentQuestion.name }</h4>
            <ul class="progress-list">
              ${ this.test.map((item, index) => `<li class="progress-list__item ${ index <= this.current ? 'is-active' : '' }"></li>`).join('') }
            </ul>
          </header>

          <ul class="answers-list">
            ${ currentQuestion.answer.map((answer, index) => {
                return `<li class="answers-list__item">
                  <label class="custom-radio-wrap">
                    <input type="radio" name="${ `radio_question_${ currentQuestion.id }` }" class="custom-radio js-custom-radio" value="" ${ index === 0 ? 'checked' : ''} data-radio-id="${ index }">

                    <span class="custom-radio__label">
                      <span class="custom-radio__radio"></span>
                      ${ Object.values(answer) }
                    </span>
                  </label>
                </li>`
            }).join('')
                }
          </ul>

          <div class="test__nav">
              ${ this.current > 0 ? '<button type="button" class="btn btn_gray test__btn test__btn_prev is-prev js-test-btn">Назад</button>' : '' }
              <button type="button" class="btn btn_default test__btn test__btn_next is-next js-test-btn">Дальше</button>
          </div>
        </div>
      `;

      $(this.testContentSel).fadeOut(200, () => {
        $(this.testContentSel).html(template).fadeIn(200);
      });
    }

    finishRender() {
      const summ = this.storage.map(item => item.points).reduce((acc, current) => acc + current);

      let currentResult = null;

      /*
        0 - 5
        5 - 9
        10 - 12
        13 - 15
      */

      if (summ < 5) {
        currentResult = this.results[0];
      } else if (summ >= 5 && summ <= 9) {
        currentResult = this.results[1];
      } else if (summ >= 10 && summ <= 12) {
        currentResult = this.results[2];
      } else if (summ > 12) {
        currentResult = this.results[3];
      }

      qs('.js-og-title').setAttribute('content', currentResult.title);
      qs('.js-og-description').setAttribute('content', currentResult.description);
      qs('.js-og-image').setAttribute('content', window.location.origin + currentResult.shareImg);

      const template = `<div class="test-results__img-wrap">
          <picture>
            <source media="(min-width: 1025px)" srcset="${ currentResult.img }">
              <source media="(min-width: 768px)" srcset="${ currentResult.imgTablet }">
              <img src="${ currentResult.imgMobile }" alt="" class="test-results__img">
          </picture>
        </div>
        <div class="test-results__content nav-pad">
          <h2 class="test-results__title">${ currentResult.title }</h2>

          <p class="test-results__text">${ currentResult.text }</p>
          <ul class="share-list">
            <li class="share-list__item js-share-item" data-share="vkontakte" data-params="${ location.origin }&&${ currentResult.title }&&${location.origin + currentResult.shareImg}&&${currentResult.text}">
              <span class="share-list__icon-wrap share-list__icon-wrap_vk">
                <svg xmlns="http://www.w3.org/2000/svg" class="share-list__icon share-list__icon_vk">
                  <use xlink:href="#vk" />
                </svg>
              </span>

              Поделиться в&nbsp;ВК
            </li>
            <li class="share-list__item js-share-item" data-share="facebook"  data-params="${ location.origin }&&${ currentResult.title }&&${location.origin + currentResult.shareImg}&&${currentResult.text}">
                <span class="share-list__icon-wrap share-list__icon-wrap_facebook">
                  <svg xmlns="http://www.w3.org/2000/svg" class="share-list__icon share-list__icon_facebook">
                    <use xlink:href="#facebook" />
                  </svg>
                </span>

                Поделиться в&nbsp;facebook
            </li>
            <li class="share-list__item js-share-item" data-share="twitter" data-params="${ location.origin }&&${ currentResult.title }">
              <span class="share-list__icon-wrap share-list__icon-wrap_twitter">
                <svg xmlns="http://www.w3.org/2000/svg" class="share-list__icon share-list__icon_twitter">
                  <use xlink:href="#twitter" />
                </svg>
              </span>

              Рассказать в&nbsp;twitter
            </li>
          </ul>

          <footer class="test-results__test-nav">
            <div class="test-results__test-nav-item">
              <span class="test-results__reset-test js-reset-test-btn">Пройти тест заново</span>
            </div>
            <div class="test-results__test-nav-item">
              <a href="#intro" class="app-section__next-slide app-section__next-slide_results">На&nbsp;главную</a>
            </div>
          </footer>
        </div>
      `;

      this.reset();
      this.setQuestion();

      qs(this.testResultsSel).innerHTML = template;
      qs(this.testSel).classList.remove('is-show');
      qs(this.testResultsSel).classList.add('is-show');
    }

    setQuestion(index = this.current) {
      if (index >= 0 && index <= this.test.length - 1) {
        this.current = index;

        this.storage.push({
          id: this.test[this.current].id,
          points: parseInt(Object.keys(this.test[this.current].answer[0])[0])
        });

        this.renderQuestion();
      } else {
        this.finishRender();
      }
    }

    setAnswer(index) {
      const item = this.storage.find(item => item.id === this.test[this.current].id);

      if (item) {
        item.points = parseInt(Object.keys(this.test[this.current].answer[index])[0]);
      }
    }

    init() {
      this.handleEvents();
      this.setQuestion();
    }
  }

  // Класс приложения
  class SchnurApp {
    constructor() {
      // Селектор fullpage.js
      this.fullpageSel = '.js-app-content';
      // Слайдер участников
      this.membersSliderSel = '.js-members-slider';

      // Слайдер участников
      this.dotdotdotSel = '.js-dotdotdot';

      // Дефолтные настройки fullpage.js
      this.fullpageOpts = {
        sectionSelector: '.js-app-section',
        navigation: true,
        navigationTooltips: [...qsA('.js-app-section')].map((item, index) => minTwoDigits(index + 1)), // числа в навигации fullpage.js
      };

      // Дефолтные настройки слайдера участников
      this.membersSliderOpts = {
        appendArrows: '.js-slider-nav',
        infinite: true,
        nextArrow: $('.js-members-slider-arrow-next'),
        prevArrow: $('.js-members-slider-arrow-prev'),
        arrows: true,
        slidesToShow: 4,
        responsive: [{
            breakpoint: 1025,
            settings: {
              slidesToShow: 6,
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
            }
          }
        ]
      };

      // Настройки dotdotdot-js
      this.dotdotdotOpts = {
        watch: true,
      };

      const test = new Test;
      test.init();
    }

    // Обработка событий
    handleEvents() {}

    // Инит приложения
    init() {
      if (window.innerWidth > APP.mobileWidth) {
        this.initFullpage();
      } else {
        // Добавляем id'шники, когда нет fullpage.js, чтобы кнопки прокручивали экран
        [...qsA('.js-app-section')].forEach((section) => {
          section.setAttribute('id', section.dataset.anchor);
        });
      }

      this.initMembersSlider();
      this.initDotdotdot();
      this.handleEvents();
    }

    // Инит fullpage.js
    initFullpage(opts = this.fullpageOpts) {
      $(this.fullpageSel).fullpage(opts);
    }

    // Инит слайдера участников
    initMembersSlider(opts = this.membersSliderOpts) {
      $(this.membersSliderSel).slick(opts);
    }

    // Инит dotdotdot-js
    initDotdotdot(opts = this.dotdotdotOpts) {
      $(this.dotdotdotSel).dotdotdot(opts);
    }
  }

  const app = new SchnurApp;
  app.init();
});
