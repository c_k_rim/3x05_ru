const
  fs = require('fs'),
  mysql = require('mysql2/promise'),
  puppeteer = require('puppeteer'),

  messages = { //Основные сообщения
    'ARGV2_ERROR': 'укажите в параметре проект с конфигурацией',
    'FILE_NOT_FOUND': 'файл не найден ',
    'TAG_NOT_FOUND': 'Тэг для парсинга не найдена',
    'DB_CONFIG_NOT_FOUND': 'Конфигурация БД не найдена',
    'TABLE_CONFIG_NOT_FOUND': 'Настройки таблицы не найдены',
    'REQUEST_LIMIT_NOT_FOUND': 'Настройки лимита запросов не найдены'
  },

  errorLog = (message) => { //Функция для вывода ошибок
    console.error(message);
    process.exit(1);
  },

  pathResolve = require('path').resolve,

  //Путь к конфигу (указывается в аргументе при запуске)
  configPath = process.argv[2] ? pathResolve(__dirname + '/config/' + process.argv[2] + '.json') : errorLog(messages.ARGV2_ERROR),

  //JSON-конфиг
  config = fs.existsSync(configPath) ? require('cjson').load(configPath) : errorLog(messages.FILE_NOT_FOUND + configPath),

  //Искомый хэштэг
  tag = config.tag ? config.tag : errorLog(messages.TAG_NOT_FOUND),

  //Данные для коннекта к БД
  connectionData = config.mysql ? config.mysql : errorLog(messages.DB_CONFIG_NOT_FOUND),

  //Таблица, в которую записываем данные
  table = config.table ? config.table : errorLog(messages.TABLE_CONFIG_NOT_FOUND),

  //SQL-запрос, для новой записи в БД
  insertSQl = 'INSERT INTO `' + table + '` (`id`, `dimensions`, `display_url`, `shortcode`, `edge_liked_by`, `taken_at_timestamp`, `comment`) VALUES (?, ?, ?, ?, ?, ?, ?);',

  //SQL-запрос для выбора количества всех записей
  selectCountSQL = 'SELECT COUNT(id) as `count` FROM `' + table + '`;',

  //SQL-запрос для выбора записей для переключалки (все записи, конечно, выводить не нужно)
  selectSlideSQL = 'SELECT * FROM `' + table + '` ORDER BY taken_at_timestamp DESC LIMIT 50;',

  containtsTag = config.other_tag, //Дополнительная фильтрация по второму хэштэгу

  replaceReg1 = /<!-- REPLCEMEPART1 -->([\s\S]*?)<!-- \/REPLCEMEPART1 -->/igm, //Regexp для первого генерируемого куска

  replaceReg2 = /<!-- REPLCEMEPART2 -->([\s\S]*?)<!-- \/REPLCEMEPART2 -->/igm, //Regexp для второго генерируемого куска

  requestLimit = config.requestLimit ? config.requestLimit : errorLog(messages.REQUEST_LIMIT_NOT_FOUND),

  viewPort = {width: 1280, height: 960}, //Настройки для нашей страницы

  //Пропис-функция writeFile
  writeFile = (path, data, opts = 'utf8') => new Promise((resolve, reject) => {
    fs.writeFile(path, data, opts, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    })
  }),

  //Промис-функция readFile
  readFile = (path, opts = 'utf8') => new Promise((resolve, reject) => {
    fs.readFile(path, opts, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    })
  }),

  //Генерируем рандомные числа для того, что бы больше походить на человека
  randomInteger = (min, max) => Math.round(min - 0.5 + Math.random() * (max - min + 1)),

  delay = (timeout) => new Promise((resolve) => { //Промис-функция delay
    setTimeout(resolve, timeout);
  }),

  declension = (val, one, two, five) => { //Функция склонения чистительных
    let number = Math.abs(val);
    number %= 100;
    if (number >= 5 && number <= 20) {
      return five;
    }
    number %= 10;
    if (number == 1) {
      return one;
    }
    if (number >= 2 && number <= 4) {
      return two;
    }
    return five;
  };

let requestCount = 0; //Общее количество проведенных запросов

async function autoScroll(page) { //Фукнция всевдо скроллинга страницы
  try {
    await page.evaluate(async () => {
      await new Promise((resolve, reject) => {
        try {
          let totalHeight = 0,
            scrollLimit = 3,
            scrollCounter = 0,
            distance = Math.round(456 - 0.5 + Math.random() * (765 - 456 + 1)),
            timer = setInterval(() => {
              scrollCounter++;
              let scrollHeight = document.body.scrollHeight;
              window.scrollBy(0, distance);
              totalHeight += distance;
              if (totalHeight >= scrollHeight || scrollCounter >= scrollLimit) {
                clearInterval(timer);
                resolve();
              }

            }, Math.round(345 - 0.5 + Math.random() * (654 - 345 + 1)));
        } catch (e) {
          reject(e);
        }
      }).catch(error => {
        console.error(error) // add catch here
      })
    })
  } catch (error) {
    console.log('Error while scrolling:', error);
  }
}

(async () => {

  //Устанавливаем соединение с БД
  const conn = await mysql.createConnection(connectionData).catch(error => console.log(`Error mysql connect: ${error}.`));

  const browser = await puppeteer.launch({ //Запускаем наш puppeteer браузер
    args: [
      '--disable-dev-shm-usage',
      '--unlimited-storage',
      '--full-memory-crash-report',
      '--no-sandbox'
    ],
    //headless: false
  });

  const page = await browser.newPage(); //Открываем страницу в браузере

  await page.setViewport(viewPort);

  page.on('response', async response => { //Функция обработки пришедших от сервера ответов
    const url = response.url();

    try {
      const req = response.request();
      const orig = req.url();

      /**
       * Если пришел ответ на запрос, заканчивающийся __a=1 или имеющий GET параметр query_hash,
       * то это то, что нам необходимо - будем парсить пришедщий json
       */
      if ((orig.indexOf('__a=1') > -1 || orig.indexOf('query_hash') > -1)
        && (orig.indexOf('include_logged_out') < 0)) {

        const text = await response.text(),
          jsonText = JSON.parse(text),
          edges = jsonText.data //Тут, в зависимости от запроса, бывает, меняется структура ответа, но немного
            ? jsonText.data.hashtag.edge_hashtag_to_media.edges
            : jsonText.graphql.hashtag.edge_hashtag_to_media.edges;

        for (let index = 0; index < edges.length; index++) { //Пробегаемся по всем записям в ответе
          let node = edges[index].node;

          if (node.__typename === 'GraphImage') { //Пост - картинка, то, что на необходимо!

            let comment = '';

            if (node.edge_media_to_caption && node.edge_media_to_caption.edges && node.edge_media_to_caption.edges[0]
              && node.edge_media_to_caption.edges[0] && node.edge_media_to_caption.edges[0].node
              && node.edge_media_to_caption.edges[0].node.text) { //Докапываемся до комментария
              comment = node.edge_media_to_caption.edges[0].node.text;
            }
            if (
              comment.indexOf(containtsTag) > -1) { //Коммент включает в себя и второй хештэг!

              await conn.execute(insertSQl, [ //Записываем в БД
                node.id,
                JSON.stringify(node.dimensions),
                node.display_url,
                node.shortcode,
                node.edge_liked_by.count,
                node.taken_at_timestamp,
                comment
              ]);
            }
          }
        }

        requestCount++;

        //Записываем полученный json-ответ в файлик @TODO можно убрать, делалось для отладки принимаемой структуры
        await writeFile(pathResolve(`${__dirname}/data/${tag}_${requestCount}.json`), text);
      }
    } catch (err) {
      console.error(`Failed getting data from: ${url}`);
      console.error(err);
    }
  });

  await page.emulateMedia('screen');

  await page.goto('https://www.instagram.com/explore/tags/' + tag + '/?__a=1'); //Идём на страницу с тэгом
  await delay(randomInteger(1001, 2001)); //Необного ждем, сейчас все свежие данные у нас уже есть

  //Теперь идем уже на настоящую страницу с тэгом, будем скроллить.
  await page.goto('https://www.instagram.com/explore/tags/' + tag + '/');

  //Погнали скроллить, больше определённого лимита это делать не будем,
  //что бы "не заподозрили"
  for (let i = 0; i <= requestLimit; i++) {

    await delay(randomInteger(1001, 2001)); //Немножко ждём

    /**
     * Подскролливаем страницу.
     * Во время скрола будут подгружаться дополнительные данные, которые мы и будем
     * "перехватывать" в callback функции page.on('response')
     */
    await autoScroll(page);
  }

  //Поскролили, закрываем страцу и браузер, большое спасибо.
  await page.close().catch(error => console.log(`Error closing page: ${error}.`));
  await browser.close();


  //Открываем "шаблонную" html-странцу, куда будем вставлять результаты нашего парсинга
  let html_page = await readFile(pathResolve(`${__dirname}/../build/index.html`)),
    [countRows] = await conn.execute(selectCountSQL), //Общее кол-во результатов
    [sliderRows] = await conn.execute(selectSlideSQL), //Выбираем несколько свежих результатов для вставки в листалку


    //Подготавливаем новый контент на страницу
    newContent = {
      'count': '<!-- REPLCEMEPART1 -->' + countRows[0].count + '&nbsp;' + declension(countRows[0].count, 'участник', 'участника', 'участников') + '<!-- /REPLCEMEPART1 -->',
      'slider': '<!-- REPLCEMEPART2 --><div class="members-slider js-members-slider">',
      '_slider_end': '</div><!-- /REPLCEMEPART2 -->'
    };

  sliderRows.forEach((item) => { //Пробегаемся по всем выбранным постам - формируем html-cлайды на страницу
    newContent.slider += `<div class="members-slider__slide">
                                <div class="members-slider__img-wrap">
                                    <img src="${item.display_url}" alt="" class="members-slider__img">
                                </div>
                                <div class="members-slider__content">
                                    <span class="members-slider__like">Нравится: ${item.edge_liked_by}</span>
                                    <span class="members-slider__text js-dotdotdot">${item.comment}</span>
                                </div>
                            </div>`;
  });

  newContent.slider += newContent._slider_end;

  //Заменяем наши блоки по ключевым словам (REPLCEMEPART) на сгенерированные html-ки
  html_page = html_page.replace(replaceReg1, newContent.count).replace(replaceReg2, newContent.slider);

  //Записываем вновь сформированный html-файл уже на своё законное место
  await writeFile(`${__dirname}/index.html`, html_page);

  process.exit();
})();
