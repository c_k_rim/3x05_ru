'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

svg4everybody();

/*

 *	jQuery dotdotdot 2.0.0

 *

 *	Copyright (c) Fred Heusschen

 *	www.frebsite.nl

 *

 *	Plugin website:

 *	dotdotdot.frebsite.nl

 *

 *	Licensed under the MIT license.

 *	http://en.wikipedia.org/wiki/MIT_License

 */

(function ($, undef) {

        'use strict';

        if ($.fn.dotdotdot) {

                return;
        }

        $.fn.dotdotdot = function (o) {

                if (this.length === 0) {

                        $.fn.dotdotdot.debug('No element found for "' + this.selector + '".');

                        return this;
                }

                if (this.length > 1) {

                        return this.each(function () {

                                $(this).dotdotdot(o);
                        });
                }

                var $window = $(window);

                var $dot = this;

                if ($dot.data('dotdotdot')) {

                        $dot.trigger('destroy.dot');
                }

                var orgContent = $dot.contents();

                $dot.data('dotdotdot-style', $dot.attr('style') || '');

                $dot.css('word-wrap', 'break-word');

                if ($dot.css('white-space') === 'nowrap') {

                        $dot.css('white-space', 'normal');
                }

                $dot.bind_events = function () {

                        $dot.on('update.dot', function (e, c) {

                                $dot.removeClass("is-truncated");

                                e.preventDefault();

                                e.stopPropagation();

                                switch (_typeof(opts.height)) {

                                        case 'number':

                                                opts.maxHeight = opts.height;

                                                break;

                                        case 'function':

                                                opts.maxHeight = opts.height.call($dot[0]);

                                                break;

                                        default:

                                                opts.maxHeight = getTrueInnerHeight($dot);

                                                break;

                                }

                                opts.maxHeight += opts.tolerance;

                                if (typeof c != 'undefined') {

                                        if (typeof c == 'string' || 'nodeType' in c && c.nodeType === 1) {

                                                c = $('<div />').append(c).contents();
                                        }

                                        if (c instanceof $) {

                                                orgContent = c;
                                        }
                                }

                                $inr = $dot.wrapInner('<div class="dotdotdot" />').children();

                                $inr.contents().detach().end().append(orgContent.clone(true)).find('br').replaceWith('  <br />  ').end().css({

                                        'height': 'auto',

                                        'width': 'auto',

                                        'border': 'none',

                                        'padding': 0,

                                        'margin': 0

                                });

                                var after = false,
                                    trunc = false;

                                if (conf.afterElement) {

                                        after = conf.afterElement.clone(true);

                                        after.show();

                                        conf.afterElement.detach();
                                }

                                if (test($inr, opts)) {

                                        if (opts.wrap == 'children') {

                                                trunc = children($inr, opts, after);
                                        } else {

                                                trunc = ellipsis($inr, $dot, $inr, opts, after);
                                        }
                                }

                                $inr.replaceWith($inr.contents());

                                $inr = null;

                                if ($.isFunction(opts.callback)) {

                                        opts.callback.call($dot[0], trunc, orgContent);
                                }

                                conf.isTruncated = trunc;

                                return trunc;
                        }).on('isTruncated.dot', function (e, fn) {

                                e.preventDefault();

                                e.stopPropagation();

                                if (typeof fn == 'function') {

                                        fn.call($dot[0], conf.isTruncated);
                                }

                                return conf.isTruncated;
                        }).on('originalContent.dot', function (e, fn) {

                                e.preventDefault();

                                e.stopPropagation();

                                if (typeof fn == 'function') {

                                        fn.call($dot[0], orgContent);
                                }

                                return orgContent;
                        }).on('destroy.dot', function (e) {

                                e.preventDefault();

                                e.stopPropagation();

                                $dot.unwatch().unbind_events().contents().detach().end().append(orgContent).attr('style', $dot.data('dotdotdot-style') || '').removeClass('is-truncated').data('dotdotdot', false);
                        });

                        return $dot;
                }; //	/bind_events


                $dot.unbind_events = function () {

                        $dot.off('.dot');

                        return $dot;
                }; //	/unbind_events


                $dot.watch = function () {

                        $dot.unwatch();

                        if (opts.watch == 'window') {

                                var _wWidth = $window.width(),
                                    _wHeight = $window.height();

                                $window.on('resize.dot' + conf.dotId, function () {

                                        var currentWwidth = $window.width();

                                        var currentWheight = $window.height();

                                        if (_wWidth != currentWwidth || _wHeight != currentWheight || !opts.windowResizeFix) {

                                                _wWidth = currentWwidth;

                                                _wHeight = currentWheight;

                                                if (watchInt) {

                                                        clearInterval(watchInt);
                                                }

                                                watchInt = setTimeout(function () {

                                                        $dot.trigger('update.dot');
                                                }, 100);
                                        }
                                });
                        } else {

                                watchOrg = getSizes($dot);

                                watchInt = setInterval(function () {

                                        if ($dot.is(':visible')) {

                                                var watchNew = getSizes($dot);

                                                if (watchOrg.width != watchNew.width || watchOrg.height != watchNew.height) {

                                                        $dot.trigger('update.dot');

                                                        watchOrg = watchNew;
                                                }
                                        }
                                }, 500);
                        }

                        return $dot;
                };

                $dot.unwatch = function () {

                        $(window).off('resize.dot' + conf.dotId);

                        if (watchInt) {

                                clearInterval(watchInt);
                        }

                        return $dot;
                };

                var opts = $.extend(true, {}, $.fn.dotdotdot.defaults, o),
                    conf = {},
                    watchOrg = {},
                    watchInt = null,
                    $inr = null;

                if (!(opts.lastCharacter.remove instanceof Array)) {

                        opts.lastCharacter.remove = $.fn.dotdotdot.defaultArrays.lastCharacter.remove;
                }

                if (!(opts.lastCharacter.noEllipsis instanceof Array)) {

                        opts.lastCharacter.noEllipsis = $.fn.dotdotdot.defaultArrays.lastCharacter.noEllipsis;
                }

                conf.afterElement = getElement(opts.after, $dot);

                conf.isTruncated = false;

                conf.dotId = dotId++;

                $dot.data('dotdotdot', true).bind_events().trigger('update.dot');

                if (opts.watch) {

                        $dot.watch();
                }

                return $dot;
        };

        //	public

        $.fn.dotdotdot.defaults = {

                'ellipsis': '\u2026 ',

                'wrap': 'word',

                'fallbackToLetter': true,

                'lastCharacter': {},

                'tolerance': 0,

                'callback': null,

                'after': null,

                'height': null,

                'watch': false,

                'windowResizeFix': true,

                'maxLength': null

        };

        $.fn.dotdotdot.defaultArrays = {

                'lastCharacter': {

                        'remove': [' ', '\u3000', ',', ';', '.', '!', '?'],

                        'noEllipsis': []

                }

        };

        $.fn.dotdotdot.debug = function (msg) {};

        //	private

        var dotId = 1;

        function children($elem, o, after) {

                var $elements = $elem.children(),
                    isTruncated = false;

                $elem.empty();

                for (var a = 0, l = $elements.length; a < l; a++) {

                        var $e = $elements.eq(a);

                        $elem.append($e);

                        if (after) {

                                $elem.append(after);
                        }

                        if (test($elem, o)) {

                                $e.remove();

                                isTruncated = true;

                                break;
                        } else {

                                if (after) {

                                        after.detach();
                                }
                        }
                }

                return isTruncated;
        }

        function ellipsis($elem, $d, $i, o, after) {

                var isTruncated = false;

                //	Don't put the ellipsis directly inside these elements

                var notx = 'a, table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, blockquote, select, optgroup, option, textarea, script, style';

                //	Don't remove these elements even if they are after the ellipsis

                var noty = 'script, .dotdotdot-keep';

                $elem.contents().detach().each(function () {

                        var e = this,
                            $e = $(e);

                        if (typeof e == 'undefined') {

                                return true;
                        } else if ($e.is(noty)) {

                                $elem.append($e);
                        } else if (isTruncated) {

                                return true;
                        } else {

                                $elem.append($e);

                                if (after && !$e.is(o.after) && !$e.find(o.after).length) {

                                        $elem[$elem.is(notx) ? 'after' : 'append'](after);
                                }

                                if (test($i, o)) {

                                        if (e.nodeType == 3) // node is TEXT

                                                {

                                                        isTruncated = ellipsisElement($e, $d, $i, o, after);
                                                } else {

                                                isTruncated = ellipsis($e, $d, $i, o, after);
                                        }
                                }

                                if (!isTruncated) {

                                        if (after) {

                                                after.detach();
                                        }
                                }
                        }
                });

                // Ensure contents does not create new line after ellipsis

                if (test($i, o)) {

                        ellipsis($i, $d, $i, o, after);
                }

                $d.addClass("is-truncated");

                return isTruncated;
        }

        function ellipsisElement($e, $d, $i, o, after) {

                var e = $e[0];

                if (!e) {

                        return false;
                }

                var txt = getTextContent(e),
                    space = txt.indexOf(' ') !== -1 ? ' ' : '\u3000',
                    separator = o.wrap == 'letter' ? '' : space,
                    textArr = txt.split(separator),
                    position = -1,
                    midPos = -1,
                    startPos = 0,
                    endPos = textArr.length - 1;

                //	Only one word

                if (o.fallbackToLetter && startPos === 0 && endPos === 0) {

                        separator = '';

                        textArr = txt.split(separator);

                        endPos = textArr.length - 1;
                }

                if (o.maxLength) {

                        txt = addEllipsis(txt.trim().substr(0, o.maxLength), o);

                        setTextContent(e, txt);
                } else {

                        while (startPos <= endPos && !(startPos === 0 && endPos === 0)) {

                                var m = Math.floor((startPos + endPos) / 2);

                                if (m == midPos) {

                                        break;
                                }

                                midPos = m;

                                setTextContent(e, textArr.slice(0, midPos + 1).join(separator) + o.ellipsis);

                                $i.children().each(function () {

                                        $(this).toggle().toggle();
                                });

                                if (!test($i, o)) {

                                        position = midPos;

                                        startPos = midPos;
                                } else {

                                        endPos = midPos;

                                        //	Fallback to letter

                                        if (o.fallbackToLetter && startPos === 0 && endPos === 0) {

                                                separator = '';

                                                textArr = textArr[0].split(separator);

                                                position = -1;

                                                midPos = -1;

                                                startPos = 0;

                                                endPos = textArr.length - 1;
                                        }
                                }
                        }

                        if (position != -1 && !(textArr.length === 1 && textArr[0].length === 0)) {

                                txt = addEllipsis(textArr.slice(0, position + 1).join(separator), o);

                                setTextContent(e, txt);
                        } else {

                                var $w = $e.parent();

                                $e.detach();

                                var afterLength = after && after.closest($w).length ? after.length : 0;

                                if ($w.contents().length > afterLength) {

                                        e = findLastTextNode($w.contents().eq(-1 - afterLength), $d);
                                } else {

                                        e = findLastTextNode($w, $d, true);

                                        if (!afterLength) {

                                                $w.detach();
                                        }
                                }

                                if (e) {

                                        txt = addEllipsis(getTextContent(e), o);

                                        setTextContent(e, txt);

                                        if (afterLength && after) {

                                                var $parent = after.parent();

                                                $(e).parent().append(after);

                                                if (!$.trim($parent.html())) {

                                                        $parent.remove();
                                                }
                                        }
                                }
                        }
                }

                return true;
        }

        function test($i, o) {

                return $i.innerHeight() > o.maxHeight || o.maxLength && $i.text().trim().length > o.maxLength;
        }

        function addEllipsis(txt, o) {

                while ($.inArray(txt.slice(-1), o.lastCharacter.remove) > -1) {

                        txt = txt.slice(0, -1);
                }

                if ($.inArray(txt.slice(-1), o.lastCharacter.noEllipsis) < 0) {

                        txt += o.ellipsis;
                }

                return txt;
        }

        function getSizes($d) {

                return {

                        'width': $d.innerWidth(),

                        'height': $d.innerHeight()

                };
        }

        function setTextContent(e, content) {

                if (e.innerText) {

                        e.innerText = content;
                } else if (e.nodeValue) {

                        e.nodeValue = content;
                } else if (e.textContent) {

                        e.textContent = content;
                }
        }

        function getTextContent(e) {

                if (e.innerText) {

                        return e.innerText;
                } else if (e.nodeValue) {

                        return e.nodeValue;
                } else if (e.textContent) {

                        return e.textContent;
                } else {

                        return "";
                }
        }

        function getPrevNode(n) {

                do {

                        n = n.previousSibling;
                } while (n && n.nodeType !== 1 && n.nodeType !== 3);

                return n;
        }

        function findLastTextNode($el, $top, excludeCurrent) {

                var e = $el && $el[0],
                    p;

                if (e) {

                        if (!excludeCurrent) {

                                if (e.nodeType === 3) {

                                        return e;
                                }

                                if ($.trim($el.text())) {

                                        return findLastTextNode($el.contents().last(), $top);
                                }
                        }

                        p = getPrevNode(e);

                        while (!p) {

                                $el = $el.parent();

                                if ($el.is($top) || !$el.length) {

                                        return false;
                                }

                                p = getPrevNode($el[0]);
                        }

                        if (p) {

                                return findLastTextNode($(p), $top);
                        }
                }

                return false;
        }

        function getElement(e, $i) {

                if (!e) {

                        return false;
                }

                if (typeof e === 'string') {

                        e = $(e, $i);

                        return e.length ? e : false;
                }

                return !e.jquery ? false : e;
        }

        function getTrueInnerHeight($el) {

                var h = $el.innerHeight(),
                    a = ['paddingTop', 'paddingBottom'];

                for (var z = 0, l = a.length; z < l; z++) {

                        var m = parseInt($el.css(a[z]), 10);

                        if (isNaN(m)) {

                                m = 0;
                        }

                        h -= m;
                }

                return h;
        }
})(jQuery);

var APP = {
        name: 'Schnur App',
        mobileWidth: 767,
        tabletWidth: 1366,
        desktopWidth: 1440,
        desktopHeight: 742
};

// Нули перед числами < 10
var minTwoDigits = function minTwoDigits(n) {
        return (n < 10 ? '0' : '') + n;
};

// Короткие алиасы для querySelector
var qs = function qs(el) {
        return document.querySelector(el);
};
var qsA = function qsA(el) {
        return document.querySelectorAll(el);
};

document.addEventListener('DOMContentLoaded', function () {
        // Шаринг
        var Share = {
                vkontakte: function vkontakte(purl, ptitle, pimg, text) {
                        var url = 'http://vkontakte.ru/share.php?';
                        url += 'url=' + encodeURIComponent(purl);
                        url += '&title=' + encodeURIComponent(ptitle);
                        url += '&description=' + encodeURIComponent(text);
                        url += '&image=' + encodeURIComponent(pimg);
                        url += '&noparse=true';

                        Share.popup(url);
                },
                facebook: function facebook(purl, ptitle, pimg, text) {
                        console.log('fb share', purl, ptitle, pimg, text);
                        FB.ui({
                                method: 'share_open_graph',
                                action_type: 'og.shares',
                                action_properties: JSON.stringify({
                                        object: {
                                                'og:url': purl,
                                                'og:title': ptitle,
                                                'og:description': text,
                                                'og:image': pimg,
                                                'og:image:width': 1200,
                                                'og:image:height': 630
                                        }
                                })
                        }, function (response) {});
                },
                twitter: function twitter(purl, ptitle) {
                        var url = 'http://twitter.com/share?';
                        url += 'text=' + encodeURIComponent(ptitle);
                        url += '&url=' + encodeURIComponent(purl);
                        url += '&counturl=' + encodeURIComponent(purl);
                        Share.popup(url);
                },
                popup: function popup(url) {
                        window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
                }
        };

        // Класс теста

        var Test = function () {
                function Test() {
                        _classCallCheck(this, Test);

                        this.current = 0;
                        this.storage = [];
                        this.testSel = '.js-test';
                        this.testContentSel = '.js-test-content';
                        this.radioSel = '.js-custom-radio';
                        this.testResultsSel = '.js-test-results';

                        this.test = [{
                                id: 1,
                                name: 'Сколько чистой воды ты пьешь в сутки?',
                                img: '/assets/img/test/bottles.png',
                                prefix: 'bottles',
                                answer: [{
                                        3: 'Я соблюдаю питьевой режим, выпиваю 1,5 – 2 литра чистой воды в сутки'
                                }, {
                                        2: '2-4 стакана в день'
                                }, {
                                        1: 'Когда как — иногда вообще за весь день могу не выпить ни капли'
                                }, {
                                        0: 'Предпочитаю напитки «с градусами»'
                                }]
                        }, {
                                id: 2,
                                name: 'В основе твоего рациона...',
                                img: '/assets/img/test/food.png',
                                prefix: 'food',
                                answer: [{
                                        3: 'Фрукты, овощи, злаки, рыба, мясо.'
                                }, {
                                        2: 'Хлебобулочные изделия, сладости.'
                                }, {
                                        1: 'Супы быстрого приготовления, чипсы, сухарики, сладости.'
                                }, {
                                        0: 'Сигаретный дым и алкоголь.'
                                }]
                        }, {
                                id: 3,
                                name: 'Сколько часов в неделю ты занимаешься спортом?',
                                img: '/assets/img/test/dumbbells.png',
                                prefix: 'dumbbells',
                                answer: [{
                                        3: 'Более 4-х часов.'
                                }, {
                                        2: '1 – 4 часа.'
                                }, {
                                        1: 'Я не трачу время на глупости.'
                                }, {
                                        0: '«Литрболл» считается спортом?'
                                }]
                        }, {
                                id: 4,
                                name: 'Сколько часов в сутки (в среднем за неделю) ты спишь?',
                                img: '/assets/img/test/alarm-clock.png',
                                prefix: 'alarm-clock',
                                answer: [{
                                        1: 'Менее 6 часов.'
                                }, {
                                        2: '6 – 7 часов.'
                                }, {
                                        3: 'Около 8 часов.'
                                }]
                        }, {
                                id: 5,
                                name: 'Как часто ты гуляешь на свежем воздухе?',
                                img: '/assets/img/test/tree.png',
                                prefix: 'tree',
                                answer: [{
                                        3: 'Ежедневно.'
                                }, {
                                        2: 'Один раз в два – три дня.'
                                }, {
                                        1: 'Хорошо, если выйду раз в неделю.'
                                }, {
                                        0: 'На свежем воздухе я только во время перекура.'
                                }]
                        }];

                        this.results = [{
                                title: 'Ты&nbsp;Шнур года этак 1998-2001',
                                img: '/assets/img/test/schnur-bad.jpg',
                                imgTablet: '/assets/img/test/schnur-bad-tablet.jpg',
                                imgMobile: '/assets/img/test/schnur-bad-mobile.jpg',
                                shareImg: '/assets/img/test/schnur-bad-social.jpg',
                                text: '&laquo;Градус&raquo; такой жизни хоть и&nbsp;высок (от&nbsp;40&nbsp;и&nbsp;выше), но&nbsp;не&nbsp;слишком греет&nbsp;&mdash; ни&nbsp;тебя самого, ни&nbsp;твоих близких. Так что, может, попробуешь по-другому? Чем ты&nbsp;рискуешь? Возьми и&nbsp;поучаствуй в&nbsp;программе &laquo;Недельный водопой&raquo; (подчеркиваем: ВОДОПОЙ)&nbsp;&mdash; спорим на&nbsp;билеты и&nbsp;гитару с&nbsp;автографом Шнура, что тебе понравится?'
                        }, {
                                title: 'Соберись! А&nbsp;пока ты&nbsp;Шнур 2009&nbsp;года...',
                                img: '/assets/img/test/schnur-normal.jpg',
                                imgTablet: '/assets/img/test/schnur-normal-tablet.jpg',
                                imgMobile: '/assets/img/test/schnur-normal-mobile.jpg',
                                shareImg: '/assets/img/test/schnur-normal-social.jpg',
                                text: 'Но, будем откровенны, бывало и&nbsp;хуже. Самое время обратить внимание на&nbsp;себя, начать восхождение к&nbsp;вершинам молодости, активности и&nbsp;здоровья. Самый простой способ для этого&nbsp;&mdash; участвовать в&nbsp;программе &laquo;Недельный водопой&raquo;, выиграть билеты на&nbsp;концерт группировки &laquo;Ленинград&raquo; или гитару с&nbsp;автографом Шнура.'
                        }, {
                                title: 'Факт&nbsp;&mdash; ты&nbsp;Шнур 2015&nbsp;года.',
                                img: '/assets/img/test/schnur-good.jpg',
                                imgTablet: '/assets/img/test/schnur-good-tablet.jpg',
                                imgMobile: '/assets/img/test/schnur-good-mobile.jpg',
                                shareImg: '/assets/img/test/schnur-good-social.jpg',
                                text: 'Достижения здорового образа жизни для вас еще впереди. Наступают светлые дни, темп выбран верный, главное не&nbsp;сорваться. А&nbsp;держаться проще в&nbsp;кругу единомышленников. Участвуй в&nbsp;программе &laquo;Недельный водопой&raquo; и&nbsp;выиграй билеты на&nbsp;концерт группировки &laquo;Ленинград&raquo; или гитару с&nbsp;автографом Шнура.'
                        }, {
                                title: 'Ты&nbsp;в&nbsp;тренде! Настоящий Шнур 2019 года розлива!',
                                img: '/assets/img/test/schnur-excellent.jpg',
                                imgTablet: '/assets/img/test/schnur-excellent-tablet.jpg',
                                imgMobile: '/assets/img/test/schnur-excellent-mobile.jpg',
                                shareImg: '/assets/img/test/schnur-excellent-social.jpg',
                                text: 'Великолепно! &laquo;Святой Источник&raquo; всем офисом завидует тебе и&nbsp;предлагает присоединиться к&nbsp;клубу таких&nbsp;же успешных здоровых людей. Да&nbsp;что там присоединиться&nbsp;&mdash; возглавить его! Смело участвуй в&nbsp;программе &laquo;Недельный водопой&raquo;, выиграй билеты на&nbsp;концерт группировки &laquo;Ленинград&raquo; или гитару с&nbsp;автографом Шнура.'
                        }];
                }

                _createClass(Test, [{
                        key: 'handleEvents',
                        value: function handleEvents() {
                                var _this = this;

                                document.addEventListener('change', function (e) {
                                        var target = e.target;
                                        var targetClassList = target.classList;

                                        if (!targetClassList.contains('js-custom-radio')) {
                                                return;
                                        }

                                        _this.setAnswer(Number(target.dataset.radioId));
                                });

                                document.addEventListener('click', function (e) {
                                        var target = e.target;
                                        var targetClassList = target.classList;

                                        if (!targetClassList.contains('js-test-btn')) {
                                                return;
                                        }

                                        _this.setQuestion(targetClassList.contains('is-prev') ? _this.current - 1 : _this.current + 1);
                                });

                                document.addEventListener('click', function (e) {
                                        var target = e.target;
                                        var targetClassList = target.classList;

                                        if (!targetClassList.contains('js-reset-test-btn')) {
                                                return;
                                        }

                                        qs(_this.testResultsSel).innerHTML = '';
                                        qs(_this.testResultsSel).classList.remove('is-show');
                                        qs(_this.testSel).classList.add('is-show');
                                });

                                $(document).on('click', '.js-share-item', function () {
                                        Share[$(this).data('share')].apply(Share, _toConsumableArray($(this).data('params').split('&&')));
                                });
                        }
                }, {
                        key: 'reset',
                        value: function reset() {
                                this.storage = [];
                                this.current = 0;
                        }
                }, {
                        key: 'renderQuestion',
                        value: function renderQuestion() {
                                var _this2 = this;

                                var currentQuestion = this.test[this.current];

                                var template = '<div class="test__img-wrap test__img-wrap_' + currentQuestion.prefix + '">\n          <img src="' + currentQuestion.img + '" alt="" class="test__img">\n        </div>\n        <div class="test__step test__step_' + currentQuestion.prefix + '">\n          <header class="test__step-head">\n            <span class="test__question-count">\u0412\u043E\u043F\u0440\u043E\u0441 <span>' + (this.current + 1) + '</span> \u0438\u0437 <span>' + this.test.length + '</span></span>\n\n            <h4 class="test__question-title">' + currentQuestion.name + '</h4>\n            <ul class="progress-list">\n              ' + this.test.map(function (item, index) {
                                        return '<li class="progress-list__item ' + (index <= _this2.current ? 'is-active' : '') + '"></li>';
                                }).join('') + '\n            </ul>\n          </header>\n\n          <ul class="answers-list">\n            ' + currentQuestion.answer.map(function (answer, index) {
                                        return '<li class="answers-list__item">\n                  <label class="custom-radio-wrap">\n                    <input type="radio" name="' + ('radio_question_' + currentQuestion.id) + '" class="custom-radio js-custom-radio" value="" ' + (index === 0 ? 'checked' : '') + ' data-radio-id="' + index + '">\n\n                    <span class="custom-radio__label">\n                      <span class="custom-radio__radio"></span>\n                      ' + Object.values(answer) + '\n                    </span>\n                  </label>\n                </li>';
                                }).join('') + '\n          </ul>\n\n          <div class="test__nav">\n              ' + (this.current > 0 ? '<button type="button" class="btn btn_gray test__btn test__btn_prev is-prev js-test-btn">Назад</button>' : '') + '\n              <button type="button" class="btn btn_default test__btn test__btn_next is-next js-test-btn">\u0414\u0430\u043B\u044C\u0448\u0435</button>\n          </div>\n        </div>\n      ';

                                $(this.testContentSel).fadeOut(200, function () {
                                        $(_this2.testContentSel).html(template).fadeIn(200);
                                });
                        }
                }, {
                        key: 'finishRender',
                        value: function finishRender() {
                                var summ = this.storage.map(function (item) {
                                        return item.points;
                                }).reduce(function (acc, current) {
                                        return acc + current;
                                });

                                var currentResult = null;

                                /*
                                  0 - 5
                                  5 - 9
                                  10 - 12
                                  13 - 15
                                */

                                if (summ < 5) {
                                        currentResult = this.results[0];
                                } else if (summ >= 5 && summ <= 9) {
                                        currentResult = this.results[1];
                                } else if (summ >= 10 && summ <= 12) {
                                        currentResult = this.results[2];
                                } else if (summ > 12) {
                                        currentResult = this.results[3];
                                }

                                qs('.js-og-title').setAttribute('content', currentResult.title);
                                qs('.js-og-description').setAttribute('content', currentResult.description);
                                qs('.js-og-image').setAttribute('content', window.location.origin + currentResult.shareImg);

                                var template = '<div class="test-results__img-wrap">\n          <picture>\n            <source media="(min-width: 1025px)" srcset="' + currentResult.img + '">\n              <source media="(min-width: 768px)" srcset="' + currentResult.imgTablet + '">\n              <img src="' + currentResult.imgMobile + '" alt="" class="test-results__img">\n          </picture>\n        </div>\n        <div class="test-results__content nav-pad">\n          <h2 class="test-results__title">' + currentResult.title + '</h2>\n\n          <p class="test-results__text">' + currentResult.text + '</p>\n          <ul class="share-list">\n            <li class="share-list__item js-share-item" data-share="vkontakte" data-params="' + location.origin + '&&' + currentResult.title + '&&' + (location.origin + currentResult.shareImg) + '&&' + currentResult.text + '">\n              <span class="share-list__icon-wrap share-list__icon-wrap_vk">\n                <svg xmlns="http://www.w3.org/2000/svg" class="share-list__icon share-list__icon_vk">\n                  <use xlink:href="#vk" />\n                </svg>\n              </span>\n\n              \u041F\u043E\u0434\u0435\u043B\u0438\u0442\u044C\u0441\u044F \u0432&nbsp;\u0412\u041A\n            </li>\n            <li class="share-list__item js-share-item" data-share="facebook"  data-params="' + location.origin + '&&' + currentResult.title + '&&' + (location.origin + currentResult.shareImg) + '&&' + currentResult.text + '">\n                <span class="share-list__icon-wrap share-list__icon-wrap_facebook">\n                  <svg xmlns="http://www.w3.org/2000/svg" class="share-list__icon share-list__icon_facebook">\n                    <use xlink:href="#facebook" />\n                  </svg>\n                </span>\n\n                \u041F\u043E\u0434\u0435\u043B\u0438\u0442\u044C\u0441\u044F \u0432&nbsp;facebook\n            </li>\n            <li class="share-list__item js-share-item" data-share="twitter" data-params="' + location.origin + '&&' + currentResult.title + '">\n              <span class="share-list__icon-wrap share-list__icon-wrap_twitter">\n                <svg xmlns="http://www.w3.org/2000/svg" class="share-list__icon share-list__icon_twitter">\n                  <use xlink:href="#twitter" />\n                </svg>\n              </span>\n\n              \u0420\u0430\u0441\u0441\u043A\u0430\u0437\u0430\u0442\u044C \u0432&nbsp;twitter\n            </li>\n          </ul>\n\n          <footer class="test-results__test-nav">\n            <div class="test-results__test-nav-item">\n              <span class="test-results__reset-test js-reset-test-btn">\u041F\u0440\u043E\u0439\u0442\u0438 \u0442\u0435\u0441\u0442 \u0437\u0430\u043D\u043E\u0432\u043E</span>\n            </div>\n            <div class="test-results__test-nav-item">\n              <a href="#intro" class="app-section__next-slide app-section__next-slide_results">\u041D\u0430&nbsp;\u0433\u043B\u0430\u0432\u043D\u0443\u044E</a>\n            </div>\n          </footer>\n        </div>\n      ';

                                this.reset();
                                this.setQuestion();

                                qs(this.testResultsSel).innerHTML = template;
                                qs(this.testSel).classList.remove('is-show');
                                qs(this.testResultsSel).classList.add('is-show');
                        }
                }, {
                        key: 'setQuestion',
                        value: function setQuestion() {
                                var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.current;

                                if (index >= 0 && index <= this.test.length - 1) {
                                        this.current = index;

                                        this.storage.push({
                                                id: this.test[this.current].id,
                                                points: parseInt(Object.keys(this.test[this.current].answer[0])[0])
                                        });

                                        this.renderQuestion();
                                } else {
                                        this.finishRender();
                                }
                        }
                }, {
                        key: 'setAnswer',
                        value: function setAnswer(index) {
                                var _this3 = this;

                                var item = this.storage.find(function (item) {
                                        return item.id === _this3.test[_this3.current].id;
                                });

                                if (item) {
                                        item.points = parseInt(Object.keys(this.test[this.current].answer[index])[0]);
                                }
                        }
                }, {
                        key: 'init',
                        value: function init() {
                                this.handleEvents();
                                this.setQuestion();
                        }
                }]);

                return Test;
        }();

        // Класс приложения


        var SchnurApp = function () {
                function SchnurApp() {
                        _classCallCheck(this, SchnurApp);

                        // Селектор fullpage.js
                        this.fullpageSel = '.js-app-content';
                        // Слайдер участников
                        this.membersSliderSel = '.js-members-slider';

                        // Слайдер участников
                        this.dotdotdotSel = '.js-dotdotdot';

                        // Дефолтные настройки fullpage.js
                        this.fullpageOpts = {
                                sectionSelector: '.js-app-section',
                                navigation: true,
                                navigationTooltips: [].concat(_toConsumableArray(qsA('.js-app-section'))).map(function (item, index) {
                                        return minTwoDigits(index + 1);
                                }) // числа в навигации fullpage.js
                        };

                        // Дефолтные настройки слайдера участников
                        this.membersSliderOpts = {
                                appendArrows: '.js-slider-nav',
                                infinite: true,
                                nextArrow: $('.js-members-slider-arrow-next'),
                                prevArrow: $('.js-members-slider-arrow-prev'),
                                arrows: true,
                                slidesToShow: 4,
                                responsive: [{
                                        breakpoint: 1025,
                                        settings: {
                                                slidesToShow: 6
                                        }
                                }, {
                                        breakpoint: 768,
                                        settings: {
                                                slidesToShow: 1
                                        }
                                }]
                        };

                        // Настройки dotdotdot-js
                        this.dotdotdotOpts = {
                                watch: true
                        };

                        var test = new Test();
                        test.init();
                }

                // Обработка событий


                _createClass(SchnurApp, [{
                        key: 'handleEvents',
                        value: function handleEvents() {}

                        // Инит приложения

                }, {
                        key: 'init',
                        value: function init() {
                                if (window.innerWidth > APP.mobileWidth) {
                                        this.initFullpage();
                                } else {
                                        // Добавляем id'шники, когда нет fullpage.js, чтобы кнопки прокручивали экран
                                        [].concat(_toConsumableArray(qsA('.js-app-section'))).forEach(function (section) {
                                                section.setAttribute('id', section.dataset.anchor);
                                        });
                                }

                                this.initMembersSlider();
                                this.initDotdotdot();
                                this.handleEvents();
                        }

                        // Инит fullpage.js

                }, {
                        key: 'initFullpage',
                        value: function initFullpage() {
                                var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.fullpageOpts;

                                $(this.fullpageSel).fullpage(opts);
                        }

                        // Инит слайдера участников

                }, {
                        key: 'initMembersSlider',
                        value: function initMembersSlider() {
                                var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.membersSliderOpts;

                                $(this.membersSliderSel).slick(opts);
                        }

                        // Инит dotdotdot-js

                }, {
                        key: 'initDotdotdot',
                        value: function initDotdotdot() {
                                var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.dotdotdotOpts;

                                $(this.dotdotdotSel).dotdotdot(opts);
                        }
                }]);

                return SchnurApp;
        }();

        var app = new SchnurApp();
        app.init();
});
//# sourceMappingURL=main.js.map
